# LightningFDTD

Three open-source Finite-Difference Time-Domain (FDTD) software libraries - Elecode, gprMax and MEEP - are tested for their applicability in lightning electromagnetic field computations. A few simple scenarios are computed using the different solvers and the results are validated by means of reference fields, which represent good approximations of the true waveforms, which the FDTD solvers should yield.
This repository provides a collection of simulation scripts, which were used to validate the open-source FDTD software libraries, the scripts including the filters that reconstruct the reference fields and the scripts which depict the comparison of the FDTD results with the reference fields for the different scenarios.

## Open-source FDTD software libraries
* Elecode \
	Repository/Documentation: https://gitlab.com/dmika/elecode 

* gprMax \
	Repository: https://github.com/gprmax/gprmax \
	Documentation: https://docs.gprmax.com/ \
	Webpage: https://www.gprmax.com 

* MEEP \
	Repository: https://github.com/nanocomp/meep \
	Webpage/Documentation: https://https://meep.readthedocs.io 

## Instructions

This instruction provides an explanation on how 
* the simulations can be run using the three different solvers
* the results can be plotted. With the provided scripts in "Results_&_Plots, the already completed simulation results can be displayed. These scripts can be adapted by the user to display own simulation results. 

### Elecode

* Visit https://gitlab.com/dmika/elecode/-/tree/cf7af3c3e12a6919f69c811fc1e990677001e984 (current compatible commit) to download Elecode and follow the instructions to install Elecode on your Linux system (if using Winows 10 or later, use the Windows Subsystem for Linux, e.g., an Ubuntu 22.04 version). 
It is sufficient to install the command line interface (CLI) version of the program.

Run the simulation of the elecode_model.elm using
```python 
	./elecode/cli/elecode <path_to_elecode_model>/elecode_model.elm
```

Depending on the input model file name (in the above example `elecode_model.elm`), the  output file is called `elecode_model.elo`. The output is sorted in columns according to the order of the field sampling routines as they occur in the simulation script. The first column represents the time column.

### gprMax
* Before simulations can be run in gprMax, the excitation file must be generated using 'simulation_scripts/lightning_return_stroke_model.py'. \
	The spatial discretization (resolution) must be specified in the script, according to the discretization that will be used in the FDTD simulation. \


	```python
	res = 50 # FDTD spatial resolution in meters 
	dt = 0.9 * res / c0 * 1/np.sqrt(3) # time step (must be exactly equal to the time step in gprMax, if the source file is exported without a time column!)
	destination_folder = "./gprMax/" 
	# ...
	rs_model.export_current_to_txt(dz_m=res, H_m = ..., t_res=dt, t_len=..., include_time_column = False, filename="{}sources.txt".format(destination_folder))
	```  
	This file must then be specified as the excitation file in the gprMax input file using the following line ('cubic extrapolate' will be ignored if 'include_time_column' was set false in the code snippet above):
	```  
	#excitation_file: sources.txt cubic extrapolate
	```  
	... if the file `sources.txt` is in the same directory as the `.in` script. Relative path denotation is also possible (e.g., `../sources.txt`, if the source file lies one folder above)
* Once the gprMax installation is completed according to the [online documentation](https://docs.gprmax.com/en/latest/include_readme.html#installation), \
and the conda environment is activated using `conda activate gprMax` , the simulation can be run using `python -m gprMax simulation_script.in`
* If a GPU shall be used, see the [instructions on how to install gprMax using CUDA](https://docs.gprmax.com/en/latest/gpu.html#extra-installation-steps-for-gpu-usage). \
Simulations are then run with the extra flag `-gpu`:  `python -m gprMax simulation_script.in -gpu`



### MEEP
MEEP can only be installed on Linux-based operating systems. Yet, a simulation can be performed while running Windows through the Windows Subsystem for Linux (WSL), for example by [installing an Ubuntu App](https://ubuntu.com/tutorials/install-ubuntu-on-wsl2-on-windows-10#1-overview).
Upon a successful installation, see [documentation](https://meep.readthedocs.io/en/latest/Installation/), the simulation scripts for MEEP can be run 'out of the box' using 
```python 
	pythonsim_script.py or
	mpirun -np 4 python sim_script.py
```
 depending on whether MEEP was installed for single-core usage only, or with mpi for multi-core simulations. In the second line of the above example the script would be run using, e.g., 4 cores. The ideal number of cores however depends on several factors, well described in the online documentation. One more important thing worth mentioning is that for comparison of simulation results (with equally dimensioned simulation domains), MEEP had to be built from source for computation using single precision floating points. The default installation comes with double precision. There is an instruction on [how to compile and install MEEP using single precision](https://meep.readthedocs.io/en/latest/Build_From_Source/) -> "Floating-Point Precision of the Fields and Materials Arrays", however the installation process depends on the Linux platform and can be a bit tedious.

