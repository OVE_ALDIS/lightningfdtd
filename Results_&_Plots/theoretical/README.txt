This folder contains the results of the EM fields for a scenario of propagation over PEC (perfectly electric conducting ground) 
using the MTLE model for the return stroke (λ = 2km, v_RS = 1.5e8 m/s) obtained from a numerical implementation of Thottappillil et al. [1997]
done by A. Dvorak in the course of a master thesis. These fields are the basis from which lossy and stratified ground scenarios are derived through
appropriate filtering. The results of the latter step are in turn used for the validation of the results obtained from open source FDTD solvers under
investigation, which are freely available for the lightning research community and ready to be reproduced by the interested reader.


[Thottappillil et al., 1997] Thottappillil, R., Rakov, V. A., and Uman,
M. A. (1997). Distribution of charge along the lightning channel: Re-
lation to remote electric and magnetic elds and to return-stroke models.
J. Geophys. Res., 102(D6):6987{7006.