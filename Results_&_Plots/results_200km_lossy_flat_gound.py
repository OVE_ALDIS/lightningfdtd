# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 19:07:40 2023

@author: kohlmann
"""
from utils.reference_field_plots import plot_reference_fields
from utils.elecode_plots import plot_elecode_results
from utils.gprmax_plots import plot_gprmax_results
from utils.meep_plots import plot_meep_results
from utils.utils import *
import matplotlib.pyplot as plt


c0 = 299792458
mu0 = 4*np.pi*1e-7
eps0 = 1/(c0**2 * mu0)

fig = plt.figure(figsize=(6.5,4.5))
ax1 = plt.gca()



distance_km = 200
time_offset_us = distance_km * 1000 / c0* 1e6
time_snippet_us = 115
kwargs = {"distance_km": distance_km,
          "h": 500, # height of the topmost layer as described in Shoory et al. 2010
          "sigma1": 1e-3,
          "sigma2": 40,
          "eps_r1": 10,
          "eps_r2": 1, # irrelevant for the considered scenarios
          "plot_PEC": False,
          "plot_filtered": True,
          "time_snippet_us": time_snippet_us,
          "file_path": "./theoretical/PEC_{}km".format(distance_km),
          "ax": ax1,}

additional_kwargs = {"plot_field": "hfield",
                    "label": "Reference (σ = 1 mS/m)",
                    "field_color": "black",
                    "axis_color": "black",
                    "linestyle": "-"
                    }

plot_reference_fields(**kwargs, **additional_kwargs)

plot_elecode_results( 
                      elecode_file="./elecode/flat_ground/270km.elo",
                      column=20, 
                      sign=1,
                      label="Elecode (1 km CPML)", 
                      field_color = "blue",
                      linestyle = "-",
                      ax=ax1)


plot_gprmax_results( 
                      gprmax_file="./gprmax/flat_ground/20230328_200km_flat_lossy_1mSm_res50m_1kmHORIPML.out",
                      field="Hy",
                      sample="rx1",
                      sign=1, 
                      label="gprMax (1 km RIPML)", 
                      field_color = "red",
                      linestyle = "-.",
                      ax=ax1)

plot_gprmax_results( 
                      gprmax_file="./gprmax/flat_ground/20230331_200km_flat_lossy_1mSm_res50m_2kmStandardRIPML.out",
                      field="Hy",
                      sample="rx1",
                      sign=1, 
                      label="gprMax (2 km RIPML)", 
                      field_color = "red",
                      linestyle = "-",
                      ax=ax1)


plot_meep_results(  results_folder= "./meep/flat_ground/flat_lossy_200km/2km_PML",
                      field="Hy",
                      sign=1, 
                      label="MEEP (2 km PML)",
                      distance = 200000,
                      underground_m = 50,
                      field_color = "green",
                      linestyle= ":",
                      ax=ax1)

plot_meep_results(  results_folder= "./meep/flat_ground/flat_lossy_200km/4km_PML",
                      field="Hy",
                      sign=1, 
                      label="MEEP (4 km PML)",
                      distance = 200000,
                      underground_m = 50,
                      field_color = "green",
                      linestyle= "-.",
                      ax=ax1)



ax1.set_xlim(time_offset_us-4, time_offset_us + time_snippet_us )
ax1.set_ylim(-0.0006, 0.004)
ax1.set_xlabel("Time (μs)")
ax1.set_ylabel(r"H$_y$ (A/m)")
ax1.grid()

ax1.legend(loc=1)    
plt.tight_layout()
plt.savefig("Fig_4a.png", dpi=300)

ax1.set_xlim(time_offset_us-4, time_offset_us + time_snippet_us/3.5 )
ax1.set_ylim(-0.0006, 0.004)

ax1.legend(loc=1)    
plt.tight_layout()
plt.savefig("Fig_4b_.png", dpi=300)

