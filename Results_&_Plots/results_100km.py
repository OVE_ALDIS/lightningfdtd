# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 19:07:40 2023

@author: kohlmann
"""
from utils.reference_field_plots import plot_reference_fields
from utils.elecode_plots import plot_elecode_results
from utils.gprmax_plots import plot_gprmax_results
from utils.meep_plots import plot_meep_results
from utils.utils import *
import matplotlib.pyplot as plt


fig = plt.figure(figsize=(6.5,4.5))
ax1 = plt.gca()

distance_km = 100
time_offset_us = distance_km * 1000 / c0* 1e6
time_snippet_us = 40
kwargs = {"distance_km": distance_km,
          "h": 500, # height of the topmost layer as described in Shoory et al. 2010
          "sigma1": 1e-3,
          "sigma2": 40,
          "eps_r1": 10,
          "eps_r2": 1, # irrelevant for the considered scenarios
          "which_field": "Ez",
          "plot_PEC": False,
          "plot_filtered": True,
          "time_snippet_us": time_snippet_us,
          "file_path": "./theoretical/PEC_{}km".format(distance_km),
          "ax": ax1,}

additional_kwargs = {"plot_field": "efield",
                    "label": "Reference (σ = 1 mS/m)",
                    "field_color": "black",
                    "axis_color": "black",
                    "linestyle": "-"
                    }

plot_reference_fields(**kwargs, **additional_kwargs)

plot_elecode_results( 
                      elecode_file="./elecode/flat_ground/100km_0001.elo",
                      column=1, 
                      sign=-1, # due to source current file still in kA instead of Ampere at the time of this simulation
                      label="Elecode (σ = 1 mS/m)", 
                      field_color = "blue",
                      linestyle = "-",
                      ax=ax1)


plot_gprmax_results( 
                      gprmax_file="./gprmax/flat_ground/20230201_3D-flat_subsRS_20kmCorridor_7kmChannel_0.5kmGround_2kmHORIPML_exceptBelowGround.out",
                      field="Ez",
                      sample="rx11",
                      sign=-1000, 
                      label="gprMax (σ = 1 mS/m)", 
                      field_color = "red",
                      linestyle = "-",
                      ax=ax1)


plot_meep_results(  results_folder= "./meep/flat_ground/flat_lossy_ground",
                      field="Ez",
                      sign=-1, 
                      label="MEEP Ez",
                      distance = 100000,
                      field_color = "green",
                      linestyle= "-",
                      ax=ax1)


plt.title(r"E$_z$ (d = {} km, σ = 1 mS/m)".format(distance_km))
ax1.set_xlim(time_offset_us-4, time_offset_us + time_snippet_us )
ax1.set_ylim(-0.5, 3)
ax1.set_xlabel("Time (μs)")
ax1.set_ylabel(r"E$_z$ (V/m)")
ax1.grid()

ax1.legend(loc=1)    
plt.tight_layout()
#plt.savefig("Fig_x.png", dpi=300)



###############################################################################
# σ = 3 mS/m
###############################################################################

fig = plt.figure(figsize=(6.5,4.5))
ax1 = plt.gca()

time_snippet_us = 25
kwargs = {"distance_km": distance_km,
          "h": 500, # height of the topmost layer as described in Shoory et al. 2010
          "sigma1": 3e-3,
          "sigma2": 40,
          "eps_r1": 10,
          "eps_r2": 1, # irrelevant for the considered scenarios
          "which_field": "Ez",
          "plot_PEC": False,
          "plot_filtered": True,
          "filtered_linestyle": "-",
          "time_snippet_us": time_snippet_us,
          "file_path": "./theoretical/PEC_{}km".format(distance_km),
          "ax": ax1,}
additional_kwargs = {"plot_field": "efield",
                    "label": "Reference",
                    "field_color": "black",
                    "axis_color": "black",
                    "linestyle": "-"
                    }

plot_reference_fields(**kwargs, **additional_kwargs)


plot_elecode_results(
                      elecode_file="./elecode/flat_ground/100km_0003.elo",
                      column=1, 
                      sign=-1, 
                      label="Elecode", 
                      field_color = "blue",
                      linestyle = "--",
                      ax=ax1)


plot_gprmax_results(  gprmax_file="./gprmax/flat_ground/20230203_GPU_3D-flat_subsRS_3mSm_10kmCorridor_5kmChannel_0.5kmGround_2kmHORIPML_exceptBelowGround.out",
                      field="Ez",
                      sample="rx11",
                      sign=-1000,# due to source current file still in kA instead of Ampere at the time of this simulation 
                      label="gprMax", 
                      field_color = "red",
                      linestyle = "-.",
                      ax=ax1)

plot_meep_results(  results_folder= "./meep/flat_ground/flat_lossy_100km",
                      field="Ez",
                      sign=-1, 
                      label="MEEP",
                      distance = 100000,
                      underground_m = 50,
                      field_color = "green",
                      linestyle= ":",
                      ax=ax1)


plt.title(r"E$_z$ (d = {} km, σ = 3 mS/m)".format(distance_km))
ax1.set_xlim(time_offset_us-1, time_offset_us + time_snippet_us )
ax1.set_ylim(-0.2, 3)
ax1.set_xlabel("Time (μs)")
ax1.set_ylabel(r"E$_z$ (V/m)")
ax1.grid()

ax1.legend(loc=1)
plt.tight_layout()
plt.savefig("Fig_5.png", dpi=300)
