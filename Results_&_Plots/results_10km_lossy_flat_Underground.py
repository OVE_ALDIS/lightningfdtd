# -*- coding: utf-8 -*-
"""
Created on Sat Mar 18 18:20:41 2023

@author: kohlmann
"""

from utils.reference_field_plots import plot_reference_fields
from utils.elecode_plots import plot_elecode_results
from utils.gprmax_plots import plot_gprmax_results
from utils.meep_plots import plot_meep_results
from utils.utils import *
import matplotlib.pyplot as plt



##################################################
################ 10 m resolution #################
##################################################


fig = plt.figure(figsize=(6.5,4.5))
ax1 = fig.gca()


distance_km = 10
time_offset_us = distance_km * 1000 / c0* 1e6
time_snippet_us = 10


kwargs = {"distance_km": distance_km,
          "h": 500, # height of the topmost layer as described in Shoory et al. 2010
          "sigma1": 1e-3,
          "sigma2": 40,
          "eps_r1": 10,
          "eps_r2": 30, 
          "plot_PEC": False,
          "underground_m": 0,
          "plot_filtered": True,
          "time_snippet_us": time_snippet_us,
          "file_path": "./theoretical/PEC_{}km".format(distance_km),
          "ax": ax1,}
additional_kwargs = {"plot_field": "efield",
                    "label": "Reference Ex (surface)",
                    "field_color": "black",
                    "axis_color": "black",
                    "linestyle": "-",
                    "linewidth": 2,
                    }
plot_reference_fields(**kwargs, **additional_kwargs)


kwargs["underground_m"] = -10
additional_kwargs["label"] = "Referene Ex (z = -10 m)"
additional_kwargs["field_color"] = "blue"

plot_reference_fields(**kwargs, **additional_kwargs)




plot_elecode_results( 
                      elecode_file="./elecode/underground/10km_res10m_ex_10mUnderground.elo",
                      column=2, 
                      sign=-1, # due to source current file still in kA instead of Ampere at the time of this simulation
                      label="Elecode Ex (surface)", 
                      field_color = "blue",
                      linestyle = ":",
                      ax=ax1)
plot_elecode_results( 
                      elecode_file="./elecode/underground/10km_res10m_ex_10mUnderground.elo",
                      column=3, 
                      sign=-1, # due to source current file still in kA instead of Ampere at the time of this simulation
                      label="Elecode Ex (z = -10 m)", 
                      field_color = "red",
                      linestyle = ":",
                      ax=ax1)

plot_gprmax_results( 
                      gprmax_file="./gprmax/underground/20230322_10km_flat_lossy_1mSm_underground_res10m.out",
                      field="Ex",
                      sample="rx1",
                      sign=-1, 
                      label="gprMax Ex (surface)", 
                      field_color = "orange",
                      linestyle = "--",
                      ax=ax1)


plot_gprmax_results( 
                      gprmax_file="./gprmax/underground/20230322_10km_flat_lossy_1mSm_underground_res10m.out",
                      field="Ex",
                      sample="rx2",
                      sign=-1, 
                      label="gprMax Ex (z = -10 m)", 
                      field_color = "green",
                      linestyle = "--",
                      ax=ax1)

meep_filedir = "./meep/underground/"
marker_config_meep = {"marker": "None",
                      "markersize": 4,
                      "markevery": 20,
                      "linestyle": "-."}


plot_meep_results(  results_folder=meep_filedir,
                      field="Ex",
                      sign=-1, 
                      label="MEEP Ex (surface)",
                      distance = 10000,
                      underground_m = 0,
                      field_color = "black",
                      **marker_config_meep,
                      ax=ax1)

plot_meep_results(  results_folder=meep_filedir,
                      field="Ex",
                      sign=-1, 
                      label="MEEP Ex (z = -10 m)",
                      distance = 10000,
                      underground_m = -10,
                      field_color = "violet",
                      **marker_config_meep,
                      ax=ax1)

ax1.set_xlim(time_offset_us-1, time_offset_us + time_snippet_us )
ax1.set_ylim(-0.5, 3)
ax1.set_xlabel("Time (μs)")
ax1.set_ylabel(r"E$_x$ (V/m)")
ax1.grid()

ax1.legend(loc=1)    
plt.tight_layout()
plt.savefig("Fig_3.png", dpi=300)



