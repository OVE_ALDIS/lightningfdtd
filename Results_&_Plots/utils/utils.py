# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 20:04:01 2023

@author: kohlmann
"""
import numpy as np

c0 = 299792458
mu0 = 4*np.pi*1e-7
eps0 = 1/(c0**2 * mu0)

def unite_legends(axes):
    """
    Unites legends of two axes in twinx plot
    
    Can be used the following way:
        
        handles, labels = unite_legends([ax1, ax2])
        leg = ax1.legend(handles, labels,loc=1, fontsize=10.5)
        leg.remove()
        ax1.add_artist(leg)
    """
    h, l = [], []
    for ax in axes:
        tmp = ax.get_legend_handles_labels()
        h.extend(tmp[0])
        l.extend(tmp[1])
    return h, l