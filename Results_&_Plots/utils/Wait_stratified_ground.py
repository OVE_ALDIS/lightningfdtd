# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 10:32:11 2022

@author: kohlmann
"""

import matplotlib.pyplot as plt
from scipy import signal, special
from scipy.fftpack import fft,ifft
#from scipy import special
import numpy as np

c0 = 299792458
mu0 = 4*np.pi*1e-7
eps0 = 1/(c0**2 * mu0)

def compute_F_str_Wait(w, dist, h1, sigma_1, sigma_2, eps_r1, eps_r2):
    """
    Computes the Wait's formulation of stratified ground
    as given in Shoory 2010
    
    Input: w ... angular frequency omega
    """
    
    gamma0 = 1j*w*np.sqrt(mu0 * eps0)
    gamma1 = np.sqrt(1j*w*mu0*(sigma_1+1j*w*eps0*eps_r1))    
    gamma2 = np.sqrt(1j*w*mu0*(sigma_2+1j*w*eps0*eps_r2))    
    u1 = np.sqrt(gamma1**2 - gamma0**2)
    u2 = np.sqrt(gamma2**2 - gamma0**2)
    K1 = u1/(sigma_1 + 1j*w*eps0*eps_r1)
    K2 = u2/(sigma_2 + 1j*w*eps0*eps_r2)

    delta_str = np.sqrt(eps0/mu0)*K1*(K2+K1*np.tanh(u1*h1))/(K1+K2*np.tanh(u1*h1))
    p_str = -0.5*gamma0*dist*delta_str**2
    F_str = 1 - 1j*np.sqrt(np.pi*p_str)*np.exp(-p_str)*special.erfc(1j*np.sqrt(p_str))

     # at frequency 0, it evaluates "to not a number", also for frequencies above about 7.5 MHz
    if h1<=20 and w/(2*np.pi) > 10e6:
        F_str = 0+0*1j
    if np.isnan(np.abs(F_str)) and np.isclose(w,0): # at frequency 0, it evaluates "to not a number", also for frequencies above about 7.5 MHz
        return 1+0*1j
    if np.isnan(np.abs(F_str)) and w>0:
        return 0+0*1j
    if np.abs(F_str) > 1e6:
        return 0+0*1j
    return F_str


def norton_filter(w, distance, sigma, eps_r):
    k0 = w*np.sqrt(eps0*mu0)
    k1 = np.sqrt(-1j*w*mu0*(sigma+1j*eps0*eps_r))
    delta1 = k0/k1*np.sqrt(1-k0**2/k1**2)
    eta = -1j*w*distance/(2*c0)*delta1**2
        
    filt = 1-1j*np.sqrt(np.pi*eta)*np.exp(-eta)*special.erfc(1j*np.sqrt(eta))
    
    if np.isnan(filt):
        return 0+0*1j
    else:
        return filt

