#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 12:08:00 2019

@author: hannes
"""

import csv
import h5py
import numpy as np
import scipy
from scipy import signal, special
from scipy.fftpack import fft,ifft
import math as m
import sys, os
import matplotlib.pyplot as plt

if __name__ != "__main__":
    
    from utils.utils import *
    from utils.Wait_stratified_ground import *
    from utils.lossy_ground import *


def plot_reference_fields(distance_km, 
                          h, 
                          sigma1, 
                          sigma2, 
                          eps_r1, 
                          eps_r2, 
                          plot_PEC = True,
                          plot_filtered = False,
                          which_field = "Ex",
                          underground_m = 0,
                          filtered_linestyle = "-",
                          time_snippet_us=20,
                          plot_field = "efield",
                          field_color="blue",
                          axis_color = "black",
                          linestyle = "-",
                          linewidth = "1.5",
                          label = "",
                          file_path="",
                          ax = None,):

    if ax==None:
        raise Exception("No axis for plotting received in plot_reference_fields. Please pass plt axis to parrameter 'ax'")
    
    distance_m = distance_km * 1000
    
    time_offset_us = distance_m/c0/1e-6
    oversampling = 1
    fields = {}
    
    
    with open(file_path) as file:
        data = csv.reader(file,delimiter="\t")
        csv_overlay = [r for r in data]
        dt = float([r[1].split(" ")[1] for r in csv_overlay[0:10] if "Unit X-Axis" in r[0]][0])
        print("Δt =", dt)
        overlay = {}
        overlay["length"] = int(csv_overlay[3][1])
        print("Length: {}".format(overlay["length"]))
        overlay["starttime"] = float(csv_overlay[4][1].strip(" s")) # time in microsec
        print("Starttime: {}".format(overlay["starttime"]))
        overlay["duration"] = overlay["length"]*dt
        overlay["time"] = np.array([i*(overlay["duration"])/overlay["length"] for i in range(overlay["length"])])
        first_data_entry = len(csv_overlay)-overlay["length"]
        overlay["efield"] = [float(row[0]) for idx, row in enumerate(csv_overlay) if idx>=first_data_entry]
        overlay["hfield"] =  [-float(row[5])/mu0 for idx, row in enumerate(csv_overlay) if idx>=first_data_entry]
        dt_oversample = dt/oversampling
        t_oversampled = np.r_[0:overlay["duration"]-oversampling*dt_oversample:dt_oversample]
        
        fields["efield"] = scipy.interpolate.interp1d(overlay["time"], overlay["efield"], "linear")
        fields["hfield"] = scipy.interpolate.interp1d(overlay["time"], overlay["hfield"], "linear")

        time_idx = np.where(overlay['time'] <= time_snippet_us*1e-6)
        overlay["efield"] = fields["efield"](t_oversampled)[time_idx]
        overlay["hfield"] = fields["hfield"](t_oversampled)[time_idx]
        overlay["time"] = t_oversampled
        time = overlay['time'][time_idx]
        dt = t_oversampled[1]-t_oversampled[0]
        
        
    d_freq = 1/dt/len(overlay['time'])
    freq = np.r_[0:1/2*1/dt:d_freq]
    w = 2*np.pi*freq
    

    if plot_PEC:
        ax.plot(time*1e6 + time_offset_us,  overlay[plot_field], color=field_color, linestyle = linestyle, linewidth=linewidth, label=label)
        ax.set_xlabel("Time (μs)")
        if plot_field == "efield":
            ax.set_ylabel("Ez (V/m)", color = axis_color)
        elif plot_field == "hfield":
            ax.set_ylabel("Hy (A/m)", color = field_color)
        ax.set_xlim(time_offset_us-0.3, time_snippet_us+time_offset_us)
        ax.set_ylim(0, 1.1*np.max(overlay[plot_field]))
        ax.tick_params(axis='y', labelcolor=field_color)



    print("*********** Plot filtered field for flat lossy ground ***********")
    
    # ######################## Compute (vertical) Ez-field above lossy ground, based on Wait's stratified ground formulation  ################################
    F_ground =  np.array(list(map(lambda omega: compute_F_str_Wait(omega,distance_m, h, sigma1, sigma2, eps_r1, eps_r2), w)))
    F_ground_upper = np.conj(F_ground[::-1])
    F_ground = np.hstack([F_ground[0:-1], F_ground_upper])
    f_ground = ifft(F_ground)

    fields["efield_filtered"] = np.convolve(overlay["efield"], f_ground, mode="full")
    fields["hfield_filtered"] = np.convolve(overlay["hfield"], f_ground, mode="full")
    
    new_field = fields["efield_filtered"]
    time_vector = (np.array(overlay["time"]))*1e6


    if which_field == "Ex":
        # ########################### NEXT STEP: COMPUTE (horizontal) Ex-Field at ground level #########################
        F_ground =  np.array(list(map(lambda omega: compute_wave_tilt_formula(omega, sigma1, eps_r1), w)))
        F_ground_upper = np.conj(F_ground[::-1])
        F_ground = np.hstack([F_ground[0:-1], F_ground_upper])
        f_ground = ifft(F_ground)

        new_field = np.convolve(fields["efield_filtered"], f_ground, mode="full")

    if underground_m < 0:
        # ########################### NEXT STEP: COMPUTE (horizontal) Ex-Field  below ground #########################
        F_ground = np.array(list(map(lambda omega: compute_rubinstein_1996_eq3(omega, sigma1, eps_r1, underground_m), w)))
        F_ground_upper = np.conj(F_ground[::-1])
        F_ground = np.hstack([F_ground[0:-1], F_ground_upper])
        f_ground = ifft(F_ground)

        new_field = np.convolve(new_field, f_ground, mode="full")



    ########################################################################
    ###################### PLOT FIELD ######################################
    ########################################################################
    
    if plot_filtered:
        
        if plot_field == "hfield":
            ax.set_xlabel("Time (μs)")
            ax.set_ylabel("Hy [A/m]")
            ax.set_xlim(time_offset_us-0.3, time_snippet_us+time_offset_us)
            ax.plot(time_vector + time_offset_us, fields["hfield_filtered"][0:len(time_vector)], color=field_color, linestyle=linestyle, linewidth=linewidth, label= label)
            plt.legend()
        else:
            ax.set_xlabel("Time (μs)")
            ax.set_ylabel("{} [V/m]".format(which_field))
            ax.set_xlim(time_offset_us-0.3, time_snippet_us+time_offset_us)
            ax.plot(time_vector + time_offset_us, new_field[0:len(time_vector)], color=field_color, linestyle=linestyle, linewidth=linewidth, label= label)            
            plt.legend()
            

if __name__ == "__main__":
    
    from Wait_stratified_ground import *
    from utils import *
    from Wait_stratified_ground import *
    from lossy_ground import *
    
    fig = plt.figure(figsize=(6,5))
    ax1 = plt.gca()
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax1.set_zorder(ax2.get_zorder()+1)
    ax1.patch.set_visible(False)
    
    distance_km = 10
    file_path = "../theoretical/PEC_{}km".format(distance_km)
    kwargs = {"distance_km": distance_km,
              "h": 20, # height of the topmost layer as described in Shoory et al. 2010
              "sigma1": 1e-3,
              "sigma2": 40,
              "eps_r1": 10,
              "eps_r2": 1, # irrelevant for the considered scenarios
              "which_field": "Ez",
              "plot_PEC": False,
              "plot_filtered": True,
              "time_snippet_us": 20,
              "plot_field": "efield",
              "field_color": "blue",
              "file_path": file_path,
              "ax": ax1,}
    
    plot_reference_fields(**kwargs)
    # ax1.legend(loc=1)    
    # ax2.legend(loc=4)