#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 23 10:53:38 2022

@author: kohlmann
"""

import h5py
import numpy as np
import matplotlib.pyplot as plt

try:
    from utils.utils import *
except:
    pass

def plot_gprmax_results(
                         gprmax_file,
                         field,
                         sample,
                         sign, 
                         label,
                         field_color,
                         linestyle = "-",
                         linewidth = 1,
                         marker = None,
                         markersize = 1,
                         markevery = 1,
                         ax=None):
        
    f = h5py.File(gprmax_file)
    
    dt = f.attrs['dt']
    
    for k in f['rxs'].keys():
        if k == sample:#eg sample = "rx1"
            h = np.array(f['rxs'][k][field])
            t = np.array([i*dt for i in range(np.shape(h)[0])])
            ax.plot(t*1e6, sign*h, 
                    color=field_color, 
                    linestyle=linestyle, 
                    linewidth=linewidth, 
                    marker=marker, 
                    markersize = markersize,
                    markevery = markevery,
                    label=label)
    
