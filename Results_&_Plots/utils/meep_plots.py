# -*- coding: utf-8 -*-
"""
Created on Fri Mar 17 09:10:38 2023

@author: kohlmann
"""

import pandas as pd
import h5py
import numpy as np
import sys, os
import matplotlib.pyplot as plt

def plot_meep_results(results_folder,
                      field,
                      sign, 
                        label,
                        field_color,
                        distance = None,
                        underground_m = None,
                        linestyle = "-",
                        linewidth = 1,
                        marker = None,
                        markersize = 1,
                        markevery = 1,
                        ax=None):
    
    try:
        filedir = results_folder
        sim_output_dir = filedir
        csv_file = [f for f in os.listdir(sim_output_dir) if f.endswith(".csv")]
        if csv_file != []:
            for file in csv_file:
                print(file)
                df = pd.read_csv(sim_output_dir+"/"+str(file))
                keys = list(df.columns.values)
                time = df['Time'].values*1e6
                for i in keys[3:]:
                    if distance != None:
                        if "("+str(distance)+"m" not in i:
                            continue
                        else:
                            if underground_m != None:
                                if ","+str(underground_m)+"m)" not in i:
                                    continue
                    field_over_time = df[i].values
                    if field in file:
                        print("field color = ", field_color)
                        ax.plot(time, field_over_time*sign, 
                                color=field_color, 
                                linestyle=linestyle, 
                                linewidth=linewidth, 
                                marker=marker, 
                                markersize = markersize,
                                markevery = markevery,
                                label=label)        

        else:
            print("No .csv file in folder!")
 
                
    except IndexError:
        print("No file specified!")
    except Exception as e:
        print(e)
        raise e
        
