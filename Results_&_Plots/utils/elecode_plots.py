# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 20:48:15 2023

@author: kohlmann
"""
import matplotlib.pyplot as plt
import csv
import numpy as np

try:
    from utils.utils import *
except:
    pass

def plot_elecode_results(elecode_file,column, 
                         sign, 
                         label,
                         field_color = None,
                         linestyle = "-",
                         linewidth = 1,
                         marker = None,
                         markersize = 1,
                         markevery = 1,
                         ax=None):
    
    elecode_time = []
    elecode_field = []

    with open(elecode_file,'r') as f:
        csvreader = csv.reader(f)
        for r in csvreader:
            spl = r[0].split()
            elecode_time.append(float(spl[0])*1e6)
            elecode_field.append(float(spl[column]))

    
    elecode_time = np.array(elecode_time)
    elecode_field = np.array(elecode_field)
    f.close()
    
    ax.plot(elecode_time, sign*elecode_field, 
            color=field_color, 
            linestyle=linestyle, 
            linewidth=linewidth, 
            marker=marker, 
            markersize = markersize,
            markevery = markevery,
            label=label)

def save_elecode_results(elecode_file,column, output_filename, field_header):
     
     elecode_time = []
     elecode_field = []

     with open(elecode_file,'r') as f:
         csvreader = csv.reader(f)
         for r in csvreader:
             spl = r[0].split()
             elecode_time.append(float(spl[0]))
             elecode_field.append(float(spl[column]))
     elecode_time = np.array(elecode_time)
     elecode_field = np.array(elecode_field)
     f.close()
     
     with open(output_filename, 'w') as f:
         f.write("t,{}\n".format(field_header))
         for idx, t in enumerate(elecode_time):
             f.write("{},{}\n".format(elecode_time[idx], elecode_field[idx]))
             
     f.close()
     
   

if __name__=="__main__":
    
    fig = plt.figure(figsize=(6,5))
    ax1 = plt.gca()
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax1.set_zorder(ax2.get_zorder()+1)
    ax1.patch.set_visible(False)
    
    plot_elecode_results( 
                         elecode_file="../elecode/100km_0001.elo",
                         column=1, 
                         sign=-1, 
                         label="Elecode (Ez)", 
                         field_color = "blue",
                         linestyle = "--",
                         ax=ax1)
    
    plot_elecode_results(
                         elecode_file="../elecode/100km_0001.elo",
                         column=2, 
                         sign=1, 
                         label="Elecode (Hy)",
                         field_color = "red",
                         linestyle = "-.",
                         ax=ax2)