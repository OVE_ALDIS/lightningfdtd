# -*- coding: utf-8 -*-
"""
Created on Sat Mar 18 18:20:41 2023

@author: kohlmann
"""

from utils.reference_field_plots import plot_reference_fields
from utils.elecode_plots import plot_elecode_results
from utils.gprmax_plots import plot_gprmax_results
from utils.meep_plots import plot_meep_results
from utils.utils import *
import matplotlib.pyplot as plt


fig = plt.figure(figsize=(6.5,4.5))
ax1 = plt.gca()

distance_km = 10
time_offset_us = distance_km * 1000 / c0* 1e6
time_snippet_us = 40
kwargs = {"distance_km": distance_km,
          "h": 500, # height of the topmost layer as described in Shoory et al. 2010
          "sigma1": 1e-3,
          "sigma2": 40,
          "eps_r1": 10,
          "eps_r2": 1, # irrelevant for the considered scenarios
          "plot_PEC": True,
          "plot_filtered": True,
          "time_snippet_us": time_snippet_us,
          "file_path": "./theoretical/PEC_{}km".format(distance_km),
          "ax": ax1,}

additional_kwargs = {"plot_field": "efield",
                    "label": "Reference (20m layer, σ = 1 mS/m)",
                    "field_color": "blue",
                    "axis_color": "black",
                    "linestyle": "-"
                    }

plot_reference_fields(**kwargs, **additional_kwargs)

meep_filedir = "./meep/flat_lossy_ground"
marker_config_meep = {"marker": "None",
                      "markersize": 4,
                      "markevery": 20,
                      "linestyle": "-."}

# plot_meep_results(  results_folder=meep_filedir,
#                       field="Ez",
#                       sign=-1, 
#                       label="MEEP Ez",
#                       distance = 10000,
#                       field_color = "red",
#                       **marker_config_meep,
#                       ax=ax1)


plot_meep_results(  results_folder=meep_filedir,
                      field="Ez",
                      sign=-1, 
                      label="MEEP Ez",
                      distance = 10000,
                      field_color = "red",
                      **marker_config_meep,
                      ax=ax1)

# plot_elecode_results( 
#                       elecode_file="./elecode/20230320_10km_20mStratifiedGround_res10m.elo",
#                       column=2, 
#                       sign=1, # due to source current file still in kA instead of Ampere at the time of this simulation
#                       label="Elecode Hy", 
#                       field_color = "blue",
#                       linestyle = "--",
#                       ax=ax1)


# plot_gprmax_results( 
#                       gprmax_file="./gprmax/50km_20m_strat.out",
#                       field="Ez",
#                       sample="rx1",
#                       sign=-1, 
#                       label="gprMax (old)", 
#                       field_color = "blue",
#                       linestyle = "-.",
#                       ax=ax1)



ax1.set_xlim(time_offset_us-4, time_offset_us + time_snippet_us )
#ax1.set_ylim(-0.5, 50)
ax1.set_xlabel("Time (μs)")
ax1.set_ylabel(r"E$_z$ (V/m)")
ax1.grid()

ax1.legend(loc=1)    
plt.tight_layout()
#plt.savefig("Fig_4.png", dpi=300)
