# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 19:07:40 2023

@author: kohlmann
"""
from utils.reference_field_plots import plot_reference_fields
from utils.elecode_plots import plot_elecode_results
from utils.gprmax_plots import plot_gprmax_results
from utils.meep_plots import plot_meep_results
from utils.utils import unite_legends
import matplotlib.pyplot as plt


fig = plt.figure(figsize=(6.5,4.5))
ax1 = plt.gca()
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax1.set_zorder(ax2.get_zorder()+1)
ax1.patch.set_visible(False)

distance_km = 1
time_snippet_us = 30


kwargs = {"distance_km": distance_km,
          "h": 500, # height of the topmost layer as described in Shoory et al. 2010
          "sigma1": 1e-3,
          "sigma2": 40,
          "eps_r1": 10,
          "eps_r2": 1, # irrelevant for the considered scenarios
          "plot_PEC": True,
          "plot_filtered": False,
          "time_snippet_us": time_snippet_us,
          "file_path": "./theoretical/PEC_{}km".format(distance_km),
          "ax": ax1,}

additional_kwargs = {"plot_field": "efield",
                    "label": "Reference Ez",
                    "field_color": "blue",
                    "axis_color": "blue",
                    "linestyle": "-",
                    "linewidth": 0.8,
                    }

plot_reference_fields(**kwargs, **additional_kwargs)
kwargs["ax"] = ax2
additional_kwargs = {"plot_field": "hfield",
                    "label": "Reference Hy",
                    "field_color": "red",
                    "axis_color": "red",
                    "linestyle": "-",
                    "linewidth": 0.8,
                    }

plot_reference_fields(**kwargs, **additional_kwargs)


handles, labels = unite_legends([ax1, ax2])
leg = ax1.legend(handles, labels,loc=1, fontsize=10.5)
leg.remove()
ax1.add_artist(leg)

add_legends = []
add_labels = []

marker_config_elecode = {"marker": "^",
                      "markersize": 4,
                      "markevery": 7,
                      "linestyle": ":"}

plot_elecode_results(
                      elecode_file="./elecode/flat_ground/1km_pec_0m.elo",
                      column=1, 
                      sign=-1, 
                      label="Elecode Ez", 
                      field_color = "blue",
                      **marker_config_elecode,
                      ax=ax1)
leg, lab = ax1.get_legend_handles_labels()

print(leg,lab)
add_legends.append(leg[-1])
add_labels.append(lab[-1])

leg = ax1.legend(add_legends, add_labels,loc=4, fontsize=10.5)
leg.remove()
#ax1.add_artist(leg)

plot_elecode_results(
                      elecode_file="./elecode/flat_ground/1km_pec_0m.elo",
                      column=2, 
                      sign=1, 
                      label="Elecode Hy", 
                      field_color = "red",
                      **marker_config_elecode,
                      ax=ax2)

leg, lab = ax2.get_legend_handles_labels()
print(leg, lab)
add_legends.append(leg[-1])
add_labels.append(lab[-1])

leg = ax1.legend(add_legends, add_labels,loc=4, fontsize=10.5)
leg.remove()


marker_config_gprmax = {"marker": "x",
                      "markersize": 4,
                      "markevery": 70,
                      "linestyle": "--"}


plot_gprmax_results(  gprmax_file="./gprmax/flat_ground/20230313_3D-flat_PEC_subsRS_1km.out",
                      field="Ez",
                      sample="rx1",
                      sign=-1, 
                      label="gprMax Ez", 
                      field_color = "blue",
                      **marker_config_gprmax,
                      ax=ax1)
leg, lab = ax1.get_legend_handles_labels()
print(leg, lab)
add_legends.append(leg[-1])
add_labels.append(lab[-1])

leg = ax1.legend(add_legends, add_labels,loc=4, fontsize=10.5)
leg.remove()


plot_gprmax_results(  gprmax_file="./gprmax/flat_ground/20230313_3D-flat_PEC_subsRS_1km.out",
                      field="Hy",
                      sample="rx1",
                      sign=1, 
                      label="gprMax Hy", 
                      field_color = "red",
                      **marker_config_gprmax,
                      ax=ax2)
leg, lab = ax2.get_legend_handles_labels()
print(leg, lab)
add_legends.append(leg[-1])
add_labels.append(lab[-1])

leg = ax1.legend(add_legends, add_labels,loc=4, fontsize=10.5)
leg.remove()


meep_filedir = "./meep/flat_ground/PEC_10_m_above_ground_2src_per_Δx"
marker_config_meep = {"marker": "o",
                      "markersize": 4,
                      "markevery": 20,
                      "linestyle": "-."}

plot_meep_results(  results_folder=meep_filedir,
                      field="Ez",
                      sign=-1, 
                      label="MEEP Ez", 
                      field_color = "blue",
                      **marker_config_meep,
                      ax=ax1)
leg, lab = ax1.get_legend_handles_labels()
print(leg, lab)
add_legends.append(leg[-1])
add_labels.append(lab[-1])

leg = ax2.legend(add_legends, add_labels,loc=4, fontsize=10.5)
leg.remove()

plot_meep_results(  results_folder=meep_filedir,
                      field="Hy",
                      sign=1, 
                      label="MEEP Hy", 
                      field_color = "red",
                      **marker_config_meep,
                      ax=ax2)
leg, lab = ax2.get_legend_handles_labels()
print(leg, lab)
add_legends.append(leg[-1])
add_labels.append(lab[-1])

leg = ax1.legend(add_legends, add_labels,loc=4, fontsize=10.5)
leg.remove()


ax1.add_artist(leg)
ax1.grid()
ax1.set_xlim(2,20)
ax1.set_ylim(-10,900)
ax2.set_ylim(-0.0131)

plt.tight_layout()
plt.savefig("Fig_2.png", dpi=300)
#ax2.grid()
