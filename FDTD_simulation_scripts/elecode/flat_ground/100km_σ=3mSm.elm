threads (12)
volume (120000, 20000, 10100, 50, 0.9)
calctime (0.5e-3)
output (1e-7)
abc (cpml, 1000, 1, 1, 1e-8, 3, 1)

block (0, 0, 0, 120000, 20000, 2000, 10, 0.003)
block (0, 0, 2000, 120000, 20000, 2000, 5.5, 0.0015)

source (current, 10000, 10000, 2000, 10000, 10000, 9000, 0, channel)
function (channel, heidler, mtle, 2000, 1.5e8, 0.25e-6, 2.5e-6, 2, 10700, 2e-6, 230e-6, 2, 6500)

calculate (ezfield, 110000, 10000, 2125)
calculate (hyfield, 110025, 10000, 2125)

