threads (12)
volume (218000, 20000, 7600, 50, 0.9)
calctime (900e-6)
output (1e-7)
abc (cpml, 1000, 1, 1, 1e-6, 3, 1)

block (0, 0, 0, 218000, 20000, 1500, 10, 0.001)
block (0, 0, 1500, 218000, 20000, 1500, 5.5, 0.0005)

source (current, 9000, 10000, 1500, 9000, 10000, 6500, 0, channel)
function (channel, heidler, mtle, 2000, 1.5e8, 0.25e-6, 2.5e-6, 2, 10700, 2e-6, 230e-6, 2, 6500)

calculate (ezfield, 209000, 10000, 1525)
calculate (hyfield, 209025, 10000, 1525)

calculate (ezfield, 209000, 10000, 1500)
calculate (hyfield, 209000, 10000, 1500)
