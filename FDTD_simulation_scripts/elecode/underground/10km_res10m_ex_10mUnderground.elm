threads (16)
volume (11000, 1200, 5650, 10)
calctime (0.10e-3)
output (1e-7)
abc (cpml, 200, 1, 1, 1e-5, 3, 1)

block (0, 0, 0, 11000, 1200, 400, 10, 0.001)
block (0, 0, 400, 11000, 1200, 400, 5.5, 0.0005)

source (current, 500, 600, 400, 500, 600, 5400, 0, channel)
function (channel, heidler, mtle, 2000, 1.5e8, 0.25e-6, 2.5e-6, 2, 10700, 2e-6, 230e-6, 2, 6500)

calculate (ezfield, 10500, 600, 405)
calculate (exfield, 10495, 600, 400)
calculate (exfield, 10495, 600, 390)
calculate (hyfield, 10495, 600, 405)

