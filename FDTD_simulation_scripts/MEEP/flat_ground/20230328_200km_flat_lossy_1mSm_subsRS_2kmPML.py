# -*- coding: utf-8 -*-
"""
Created on Fri Mar 17 11:50:05 2023

@author: kohlmann
"""

import meep as mp
import numpy as np
import pandas as pd 

c0 = 299792458
mu0 = 4*np.pi*1e-7
eps0 = 1/(c0**2 * mu0)

a = 1000 # MEEP cell unit (interpretable as "one MEEP spatial unit equals 1000m = 1km")
i0 = 1
dx_m = 50 # FDTD Δx in meters
resolution = a/dx_m
t_conversion_factor = a/c0

E_mp_to_SI_factor = i0/(a*c0*eps0)
H_mp_to_SI_factor = i0/a


courant_factor = 0.9*1/np.sqrt(3)

####################################################
######### RS CURRENT PARAMS AND MTLE MODEL #########
####################################################

stroke = [{"i":10.7e3, "tau1":0.25e-6, "tau2":2.5e-6, "n":2},
          {"i":6.5e3, "tau1":2e-6, "tau2":230e-6, "n":2}] 
mtle_lam = 2000 # MTLE model λ
channel_len_m = 5000
v_RS = 1.5e8

####################################################
######### CELL CREATION ############################
####################################################

PML_thickness_m = 2000
ground_thickness_m = 500

cell_dim_m = np.array([220000, 
                   18000+2*PML_thickness_m, 
                   channel_len_m + 1*PML_thickness_m + ground_thickness_m+100])

mp_cell = cell_dim_m/a
center_coord = mp_cell/2 # shifts the center coordinate in meep


########################################################
########    CREATE SOURCE FUNCTION AND SOURCES #########
########################################################

sources_per_dx = 2 # turns out to reduce ringing in fields due to MEEP's interpolation (see quasi-continuity of space)
total_nr_src = int(channel_len_m/dx_m * sources_per_dx)
source_extent_m = dx_m/sources_per_dx

src_center_m = np.array([PML_thickness_m + 8000, # start 2km apart from PML boundary
                       cell_dim_m[1]/2, # at the center of the y-axis
                       ground_thickness_m]) # channel base at ground level

def RS_current(height_m, t_shift):
    """
    returns a function wrapper that MEEP can call
    """
    
    mtlm_factor = np.exp(-height_m/mtle_lam)
    def wrapper(t):
        """
        Since MEEP will call this function, the time parameter comes in MEEP time unit
        which thus must be converted to seconds
        """
        t_shift = height_m/v_RS
        t = t*t_conversion_factor-t_shift

        if t < 0:  return 0
        
        current_function = 0
        for idx, i_params in enumerate(stroke): # creates Heidler-type current function with 2 terms
            eta = np.exp(-i_params["tau1"]/i_params["tau2"]*(i_params["n"] \
                             *i_params["tau2"]/i_params["tau1"])**(1/i_params["n"]))
            t_i = (t/i_params["tau1"])**i_params["n"]
            current_function += i_params["i"]/eta*t_i/(t_i+1) * np.exp(-t/i_params["tau2"])
        
        return mtlm_factor*current_function
    return wrapper

sources = []
for i in range(total_nr_src): # create source array
    h_m = i*source_extent_m # z in m
    start_time = h_m/v_RS
    factor = 1
    src_ctr = np.array([src_center_m[0]/a, src_center_m[1]/a, (src_center_m[2]+h_m)/a])
    sources.append(mp.Source(mp.CustomSource(RS_current(height_m=h_m, 
                                                        t_shift=start_time),
                                             start_time = start_time/t_conversion_factor, # to MEEP time units
                                             ),
                            amplitude = 1, #amplitude scaling not necessary, already considered in RS_current
                            component=mp.Ez, # this corresponds to the current density J in z-direction
                            center=src_ctr, 
                            size=(0,0,source_extent_m/a)))

####################################################
######### MATERIAL & GEOMETRY CREATION #############
####################################################


epsr = 10
mp_conductivity_scaling = a/(eps0*c0*epsr)
groundmaterial = mp.Medium(epsilon=epsr, D_conductivity=1e-3*mp_conductivity_scaling) 

geometry = [mp.Block(center=np.array([cell_dim_m[0]/2, cell_dim_m[1]/2, ground_thickness_m/2])/a, 
                     size=np.array([cell_dim_m[0], cell_dim_m[1], ground_thickness_m])/a,  
                     material=groundmaterial)]

####################################################
######### PML boundaries ###########################
####################################################
# boundary conditions: perfectly matched layer
pml_layers = [mp.PML(thickness=PML_thickness_m/a, direction=mp.X, side=mp.High), 
              mp.PML(thickness=PML_thickness_m/a, direction=mp.X, side=mp.Low),
              mp.PML(thickness=PML_thickness_m/a, direction=mp.Y, side=mp.High), 
              mp.PML(thickness=PML_thickness_m/a, direction=mp.Y, side=mp.Low),
              mp.PML(thickness=PML_thickness_m/a, direction=mp.Z, side=mp.High),
              #mp.PML(thickness=PML_thickness_m/a, direction=mp.Z, side=mp.Low), # not necessary due to ground, which absorbs
              ] 

####################################################
######### Field sampling ###########################
####################################################

output_fields = ["Ex", "Ez", "Hy"]
sampling_distance_m = [50000, 100000, 150000, 200000] # field outputs for multiple distances
sampling_above_ground_m = [50, 0, -10, -20, -30, -40, -50] # sampling underground
output_dir = "./"
output_filename = "20230328_3D_200km_flat_lossy_1mSm_subsRS_2kmPML"


# ******************************************************
# initialize dictionary for field output file
fields = {}

for field in output_fields:
    fields[field] = {"MEEP_Time": [],
                 "Time": []}
    for distance_m in sampling_distance_m:
        for height in sampling_above_ground_m:
            key = field+"_("+str(distance_m)+"m," + str(height) + "m)"
            fields[field][key] = []
        
field_component_meep =  {"Ex": mp.Ex, "Ey": mp.Ey, "Ez": mp.Ez,
                        "Hx": mp.Hx, "Hy": mp.Hy,"Hz": mp.Hz,
                        }

def output_field(sim):
    """
    This outputs the specified fields
    """
    mp_t = sim.meep_time()
    t = mp_t*t_conversion_factor

    for field in output_fields:
        fields[field]["MEEP_Time"].append(mp_t)
        fields[field]["Time"].append(t)
        for distance_m in sampling_distance_m:
            for height in sampling_above_ground_m:
                key = field+"_("+str(distance_m)+"m," + str(height) + "m)"
                r = np.array([(src_center_m[0]+distance_m)/a, 
                              mp_cell[1]/2,
                              src_center_m[2]/a + height/a])
                
                val_mp = np.real(sim.get_field_point(
                                    field_component_meep[field], # picks the "mp.Ez" for example
                                    r, # sampling point
                                    ))
                key = field+"_("+str(distance_m)+"m," + str(height) + "m)"
                if field.find("E") != -1:
                    mp_to_SI = E_mp_to_SI_factor
                else:
                    mp_to_SI = H_mp_to_SI_factor
                fields[field][key].append(mp_to_SI*val_mp)
            
def write_fields_to_file(sim):
    for field in output_fields:
        data = pd.DataFrame.from_dict(fields[field])
        csv_path = output_dir + "/"+output_filename+"_"+field+".csv"
        data.to_csv(csv_path)
        
sim_config = {"cell_size": mp_cell,
              "boundary_layers": pml_layers,
              "sources": sources,
              "resolution": resolution,
              "Courant": courant_factor,
              "dimensions": 3,
              "eps_averaging": True,
              "geometry": geometry,
              "geometry_center": center_coord,
              }

sim = mp.Simulation(**sim_config)

run_until = 900e-6/t_conversion_factor
field_output_at_every_mpt = 0.01
sim.run(
    mp.at_every(field_output_at_every_mpt, output_field),
    mp.at_every(run_until/20, write_fields_to_file),
    until = run_until) # write fields every fraction of total simulation time
