# -*- coding: utf-8 -*-
"""
Created on Mon Mar 13 10:33:19 2023

@author: kohlmann
"""

import numpy as np
import scipy
from scipy import interpolate

import csv
import math as m
import matplotlib.pyplot as plt

c0 = 299792458

class RS_Model:
    def __init__(self, src_configs, t_ord_of_magnitude=1e-6):
        """
        i_params is a list of dicts, which all hold Heidler function parameters: 
        {"i":1, "tau1":1.8, "tau2":95,"n":2,"t_ord_of_magn":"mus"}

        
        src_configs is structured this way:
        src_configs = {"src_x": src_x,
           "surface_z": surface_z,
           "v_rs": v_rs,
           }
        """
        self.i_params = src_configs["channel_base_current"]
        if type(self.i_params) is str:
            self.current_type = "real"
            self.filename = self.i_params
            self.readFile()
        else:
           self.current_type = "analytical"
           self.filename = None

        self.t_ord_of_magnitude = t_ord_of_magnitude
        self.src_configs = src_configs
        self.mtlm = self.src_configs["mtlm"]
        self.mtlm["lambda"] = self.mtlm["lambda"]
            
    def readFile(self):
        time = []
        amplitude = []
        with open(self.filename, "r") as f:
            data = csv.reader(f, delimiter=',')
            for row in data:
                time.append(float(row[0]))
                amplitude.append(float(row[1]))
        f.close()

        time = np.array(time)
        amplitude = np.array(np.multiply((-1),amplitude))
        # Interpolation function of real current:
        self._current_interp = interpolate.interp1d(time, amplitude, kind='cubic')
        
    
    def stroke(self):
        print("Subs. stroke (SU)\n: " + str(self.i_params))
        
    def mtlm_factor(self, height):
        """
        height must come in MEEP units
        """
        if self.mtlm == {}:
            return 1
        
        if self.mtlm["lambda"]==0:
            return 1
        
        if self.mtlm["type"] == "exp":
                return m.exp(-height/self.mtlm["lambda"])
        elif self.mtlm["type"] == "lin":
            fact = (self.mtlm["lambda"]-height)/self.mtlm["lambda"]
            if fact>0:
                return fact
            else:
                return 0
        
    def _current_fct(self, height, t_shift, t_shift_heaviside=0):
        """
        t_shift_heaviside is an extra time retardation function to check (t<t_shift_heaviside) when the source may activate (e.g. if two step functions corcur)

        """
        def wrapper(t):
            eta = []
            t_i = [] # this is just a variable substitution used for the function
            current_function = 0
            t_orig = t
            t = t_orig-t_shift
           
            # return values right away before even checking which type of function, if time certain criteria are satisfied
            if t<0:  return 0
            if t<t_shift_heaviside-t_shift:  return 0
            
            # Set mtlm_factor (can also be checked in advance, since it is applicable to every model!)

            mtlm_factor = self.mtlm_factor(height)
            
            if self.current_type == "analytical":
                for idx, i_params in enumerate(self.i_params):
                    eta.append(m.exp(-i_params["tau1"]/i_params["tau2"]*(i_params["n"] \
                                     *i_params["tau2"]/i_params["tau1"])**(1/i_params["n"])))
                    t_i.append((t/i_params["tau1"])**i_params["n"])
                    current_function += i_params["i"]/eta[idx]*t_i[idx]/(t_i[idx]+1) * m.exp(-t/i_params["tau2"])
                
                return mtlm_factor*current_function
                
            elif self.current_type=="real":
                return mtlm_factor*self._current_interp(t)
        return wrapper
    

    def plot_sources(self, h, t):
        """
        Plots current at given height and for a time array t (in seconds)
        """

        sc = self.src_configs
        v_rs = sc["v_rs"]
       
        current = np.zeros(np.shape(t))

        start_time = h/(v_rs/c0)
        current = list(map(self._current_fct(height=h, t_shift=start_time), t))
                
        plt.plot(t,current)
        plt.legend("Height = {}m".format(h))
        
        ax = plt.gca()
        #        plt.title(r"Source current function:" +
        #                  "\n" +
        #                  r"$f_1$: $i_1 = 10.7 kA$, $\tau_{11} = 0.25\mu s$, $\tau_{12}=2.5\mu s$ " +\
        #                  "\n"+
        #                  r"$f_2$: $i_2 = 6.5kA$, $\tau_{21}=2.0\mu s$, $\tau_{22}=230\mu s$")
        
        ax.set_xlabel("Time (μs)")
        ax.set_ylabel("Magnitude (kA)")
        plt.grid()
        plt.show()
    # ************************************************************************
        
    def plot_current(self, t_len, t_res, legend_label, y_label, t_shift=0, normalize = False, figure="new", ax=None, fig_width=5.2):
        """
        t_len in mus (microseconds) will be converted to meep units
        """
        #plt.figure()
        t = np.array([i*t_res  for i in range(int(t_len/t_res))])
        
        
        current = np.array(list(map(self._current_fct(height=0,t_shift=t_shift), t))) # nice, this works now pretty nicely because the wrapper gives me back a function ;)
        curr_wrapper = interpolate.interp1d(t,current, kind='cubic')
        
        #interp_curr = interpolate.approximate_taylor_polynomial(curr_wrapper, 1, 15, 1, order = 17)
        if figure=="new":
            fig = plt.figure()
            ratio = fig_width/3.5
            fontsize=14
            legend_fontsize=10.5
            fig.set_size_inches(fig_width, fig_width*4/5)    
            plt.rcParams.update({'font.family':'Arial', 'font.size':fontsize*(1+0.07*ratio)})
            ax = plt.gca()
        elif figure=="same":
            ax = ax
            
        ax.plot(t*1e6, current/np.max(current) if normalize==True else current/1000, label=legend_label)
        #plt.plot(t, interp_curr(t), label="interpolated")
        #plt.plot(t, t_res*np.cumsum(current))
        ax.legend(fontsize=12)
        ax = plt.gca()
#        plt.title(r"Source current function:" +
#                  "\n" +
#                  r"$f_1$: $i_1 = 10.7 kA$, $\tau_{11} = 0.25\mu s$, $\tau_{12}=2.5\mu s$ " +\
#                  "\n"+
#                  r"$f_2$: $i_2 = 6.5kA$, $\tau_{21}=2.0\mu s$, $\tau_{22}=230\mu s$")

        ax.set_xlabel("Time (μs)")
        ax.set_ylabel(y_label)
        ax.set_xlim(0, t_len*1e6)
        ax.grid()
        plt.tight_layout()
        plt.show()
            
        return ax

    def export_current_to_txt(self, dz_m, H_m, t_res, t_len, t_shift=0, include_time_column = True, filename="sources.txt"):
        """
        t_len and t_res in mus (microseconds) will be converted to meep units
        H_m is the channel height in meters
        dz_m is in meters
        """
        #plt.figure()
        Nz = int(H_m/dz_m)
        t = np.array([i*t_res  for i in range(int(t_len/t_res))])
        
        current = list(map(self._current_fct(height=0,t_shift=t_shift), t))
        lam = self.src_configs["mtlm"]["lambda"]

        tot_current = []
        with open(filename, "w") as f:
            if include_time_column:
                f.write('time ')
            for z in range(Nz):
                f.write("I_"+str(z*dz_m)+" ")
                tot_current.append([])
            f.write("\n")
            
            for idx, c in enumerate(current):
                if include_time_column:
                    f.write(str(t[idx])+" ")
                for z in range(Nz):
                    h = z*dz_m
                    h_fact = np.exp(-h/lam)

                    f.write(str(h_fact*c)+" ")
                    tot_current[z].append(h_fact*c)

                f.write("\n")
        
        f.close()

    def plot_current_fft(self, t_len, t_res, legend_label, y_label, from_fq=10**3, to_fq=10**6, dB=False, figure="new", ax=None, fig_width=5.2):
        
        t = np.array([i*t_res  for i in range(int(t_len/t_res))])
        
        
        current = list(map(self._current_fct(height=0,t_shift=0), t)) # nice, this works now pretty nicely because the wrapper gives me back a function ;)

        yf = np.fft.fft(current)
        yf = yf
        freqs = np.fft.fftfreq(len(current),(t[1]-t[0]))
        #xf = np.fft.fftshift(freqs)
        N = len(freqs)
        
        if dB:
            log_yf = 20*np.log10(np.abs(2.0/N*yf[0:N//2]))
            log_yf -= log_yf[2]

        else:
            log_yf =np.abs(2.0/N*yf[0:N//2])

        if figure=="new":
            fig = plt.figure()
            ratio = fig_width/3.5
            fontsize=14
            legend_fontsize=10.5
            fig.set_size_inches(fig_width, fig_width*4/5)    
            plt.rcParams.update({'font.family':'Arial', 'font.size':fontsize*(1+0.07*ratio)})
            ax = plt.gca()
        elif figure=="same":
            ax = ax
        if dB:
            ax.semilogx(freqs[0:len(log_yf)]/1000, log_yf, label=legend_label)
            ax.set_xlabel("Frequency (kHz)")
            ax.set_ylabel(y_label)
            ax.set_xlim(from_fq/1e3, to_fq/1e3)
            ax.set_ylim(-100, 5)
        else:
            ax.loglog(freqs[0:len(log_yf)]/1000, log_yf, label=legend_label)
            ax.set_xlabel("Frequency (kHz)")
            ax.set_ylabel(y_label)
            ax.set_xlim(from_fq/1e3, to_fq/1e3)
            ax.set_ylim(10e-3, 3)
        ax.legend(fontsize=12)
        plt.tight_layout()
        ax.grid()
        return ax
        

if __name__ == "__main__":
    
    firstRS = [{"i":28e3, 
             "tau1":1.8-6, 
             "tau2":95-6,
             "n":2,
                 }]
    
    subsRS = [{"i":10.7e3, 
             "tau1":0.25e-6, 
             "tau2":2.5e-6,
             "n":2,
             },
             {"i":6.5e3, 
             "tau1":2e-6,
             "tau2":230e-6, 
             "n":2,
             }]
   
    mtlm = {
            "type":"exp",
            "lambda":2000,
            }
    src_configs = {"src_x": 0,
                   "surface_z": 0, #the surface height (z) of the src x location, which can be offset by src_offset.
                   "mtlm": mtlm,
                   "v_rs": 1.5, 
                   "stroke_length": 8,
                   "source_component": None,
                   "channel_base_current": None,
                   }

    src_configs["channel_base_current"] = subsRS

    rs_model = RS_Model(src_configs=src_configs.copy(), 
                          t_ord_of_magnitude=1)

    #rs_model.plot_current(50e-6, 0.05e-6, 'Subsequent RS', 'Current (kA)', figure="new", normalize=False)
    rs_model.plot_current_fft(400e-6, 0.005e-6, 'Subsequent RS (FFT)', 'Current (kA/Hz)', from_fq=1000, to_fq=1e6, dB=True, figure="new")
    #rs_model.export_current_to_txt(dz_m=5, H_m = 5000, t_res=0.1e-6, t_len=30e-6, include_time_column = False, filename="sources_subsRS_short.txt")
    
    res = 50
    dt = 0.9 * res / c0 * 1/np.sqrt(3)
    print("Resolution: ",res,"m", "dt =", dt, "s")
    #rs_model.export_current_to_txt(dz_m=res, H_m = 5000, t_res=dt, t_len=200e-6, include_time_column = False, filename="sources_subsRS_res{}m.txt".format(res))


